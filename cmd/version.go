package cmd

import (
	"log"

	"github.com/spf13/cobra"
)

var (
	// version is the sematic version string of this app, set by the
	// build system.
	version = ``

	// revision is the id of the changeset from the version control
	// system, set by the build system.
	revision = ``

	versionCmd = &cobra.Command{
		Use:   `version`,
		Short: `Print version info.`,
		Long:  `Print version info.`,
		Run: func(cmd *cobra.Command, args []string) {
			log.Println(version, revision)
		},
	}
)

func init() {
	rootCmd.AddCommand(versionCmd)
}
