package cmd

import (
	"github.com/spf13/cobra"
)

var (
	connectCmd = &cobra.Command{
		Use:   `connect`,
		Short: `Connect to a server.`,
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
		},
	}
)

func init() {
	rootCmd.AddCommand(connectCmd)
}
