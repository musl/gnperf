package cmd

import (
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/spf13/cobra"
)

var (
	// appName is the identifier used for configuration and log
	// messages.
	appName = ``

	rootCmd = &cobra.Command{
		Use:   appName,
		Short: `A network performance testing tool.`,
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			// The default command is Help.
			cmd.Help()
			os.Exit(0)
		},
	}
)

func init() {}

// Execute sets up anything needed at runtime before the root command is
// dispatched and then dispatches it.
func Execute() error {
	rand.Seed(time.Now().UnixNano())

	log.SetOutput(os.Stderr)
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
	log.SetPrefix(appName + ` `)

	return rootCmd.Execute()
}
