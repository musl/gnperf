package main

import (
	"gitlab.com/musl/gnperf/cmd"
)

func main() {
	cmd.Execute()
}
