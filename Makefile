BIN := $(shell basename $(CURDIR))
VERSION := $(shell git describe --abbrev=0 --tags || echo 'v0.0.0')
REVISION := $(shell git rev-list -1 HEAD || echo 'NONE')

.PHONY: all clean

all: clean test $(BIN)

clean:
	rm -f $(BIN)
	go clean .

test:
	go test -v ./...

$(BIN):
	go build -o $@ -ldflags '\
		-X gitlab.com/musl/gnperf/cmd.version=$(VERSION)\
		-X gitlab.com/musl/gnperf/cmd.revision=$(REVISION)\
		-X gitlab.com/musl/gnperf/cmd.appName=$(BIN)\
		' .

.PHONY: all clean tools

